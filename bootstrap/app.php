<?php
session_start();

require '../vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
        'db' => [
            'driver' => 'mysql',
            'host' => '192.168.1.67',
            'database' => 'kloon_phamngocanh',
            'username' => 'phamngocanh',
            'password' => 'phamngocanh',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]
    ],

]);

$container = $app->getContainer();

$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

$container['view']= function($container){
    $view = new \Slim\Views\Twig('../resources/views',[
        'cache' => false,
        ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
        ));
    return $view;
};

$container['HomeController']= function($container){
  
    return new \App\Controllers\HomeController($container);
};

require '../app/routes.php';



